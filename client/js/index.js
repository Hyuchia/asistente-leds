import './font-awesome.min.js';

import { $_ready, Request, $_, Text } from '@aegis-framework/artemis';

const api = 'http://localhost:3000';


let context;

let disabled = false;

const audio = new Audio ();
let microphone = null;
let audioStream = null;

let [r, g, b, a] = [0, 0, 0, 255];

function render () {
	$_('[data-color="rojo"]').style ('color', `rgba(${r},0,0,${a})`);
	$_('[data-color="verde"]').style ('color', `rgba(0,${g},0,${a})`);
	$_('[data-color="azul"]').style ('color', `rgba(0,0,${b},${a})`);
	$_('[data-color="result"]').style ('color', `rgba(${r},${g},${b},${a})`);
}

function tts (text) {
	Request.post (`${api}/synthesize`, { text }, {
		headers: {
			'Content-Type': 'application/json'
		}
	}).then ((data) => {
		return data.blob ();
	}).then ((data) => {
		if (audio.isPlaying) {
			audio.stop ();
		}
		audio.src = window.URL.createObjectURL(data);
		audio.play ().catch (e => console.error(e));
	});
}

function hexToRgb (hex) {
	// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
	const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	hex = hex.replace(shorthandRegex, (m, r, g, b) => r + r + g + g + b + b);

	const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	r = parseInt(result[1], 16);
	g = parseInt(result[2], 16);
	b = parseInt(result[3], 16);

	render ();
}

function up (color) {
	setOne (color, 255);
}

function down (color) {
	setOne (color, 0);
}

function setOne (color, value) {
	switch (color) {
		case 'rojo':
			r = value;
			break;
		case 'verde':
			g = value;
			break;
		case 'azul':
			b = value;
			break;
	}
	render ();
}

function blink (color) {
	$_(`[data-color="${color}"]`).toggleClass ('blink');
}

function handleInput (input) {
	if (input !== '') {
		$_('[data-content="chat"]').append(`<div class="row__colum row__column--phone--8 text--right"><div class="input">${input}</div></div>`);
	}
	//input = Text.friendly (input).replace (/-/g, ' ');

	$_('input').value ('');
	const chat = $_('[data-content="chat"]').get (0);
	chat.scrollTop = chat.scrollHeight;
	const conversation = {
		dialog: input
	};

	if (context) {
		conversation.context = context;
	}

	Request.post(`${api}/converse`, conversation, { headers: { 'Content-Type': 'application/json' }}).then(response => response.json()).then((data) => {
		const responses = data.output.generic;
		context = data.context;
		for (const response of responses) {
			if (response.text) {
				tts(response.text);
				$_('[data-content="chat"]').append(`<div class="row__colum row__column--phone--8"><div class="response">${response.text.replace(/(\r\n\t|\n|\r\t)/gm, '<br>')}</div></div>`);
			} else if (response.title && response.options) {
				$_('[data-content="chat"]').append(`
					<div class="row__colum row__column--phone--8">
						<div class="response">${response.title}<br>${response.options.map(opt => `<span data-choice="${opt.value.input.text}"><i class="far fa-circle"></i> ${opt.label}</span>`).join ('<br>')}</div>

					</div>
				`);
			}
		}

		if (data.actions) {
			const action = data.actions[0];

			const name = action.name;


			if (action && name) {
				if (name === 'up') {
					const led = action.parameters.colors;
					up (led);
				} else if (name === 'down') {
					const led = action.parameters.colors;
					down (led);
				} else if (name === 'set') {
					const hex = action.parameters.hex;
					hexToRgb(hex);
				} else if (name === 'setOne') {
					const { color, value } = action.parameters;
					setOne (color, value);
				} else if (name === 'blink') {
					const { color } = action.parameters;
					blink (color);
				} else if (name === 'stopBlink') {
					const { color } = action.parameters;
					blink (color);
					down (color);
				}
			}
		}

		const chat = $_('[data-content="chat"]').get (0);
		chat.scrollTop = chat.scrollHeight;
		disabled = false;
	});
}

// When the page is ready, you can start modifying its DOM!
$_ready(() => {
	render ();
	handleInput ('');
	$_('form').submit ((event) => {
		event.preventDefault();
		if (!disabled) {
			disabled = true;
			const input = $_('input').value ();
			handleInput (input);
		}
	});

	$_('[data-content="chat"]').on ('click', '[data-choice], [data-choice]', function () {
		handleInput ($_(this).data ('choice'));
	});

	$_('[data-action="record"]').click (function () {
		if (microphone !== null) {
			microphone.stop ();
			audioStream.getAudioTracks()[0].stop();
			audioStream = null;
			$_(this).html ('<span class="fa fa-microphone"></span>');
		} else {
			navigator.mediaDevices.getUserMedia({audio: true}).then((stream) => {
				audioStream = stream;
				// store streaming data chunks in array
				const chunks = [];
				// create media recorder instance to initialize recording
				microphone = new MediaRecorder(stream);
				// function to be called when data is received
				microphone.ondataavailable = e => {
					// add stream data to chunks
					chunks.push(e.data);
					// if recorder is 'inactive' then recording has finished
					if (microphone.state == 'inactive') {
						// convert stream data chunks to a 'webm' audio format as a blob
						const blob = new Blob(chunks, { type: 'audio/ogg' });
						// convert blob to URL so it can be assigned to a audio src attribute

						const src = URL.createObjectURL(blob);
						//createAudioElement(src);
						var reader = new window.FileReader();
						reader.readAsDataURL(blob);
						reader.onloadend = function () {
							Request.post (`${api}/transcribe`, {
								audio: reader.result
							}, {
								headers: {
									'Content-Type': 'application/json'
								}
							}).then (data => data.json()).then ((data) => {
								const playback = new Audio (src);
								playback.play ();
								$_('input').value (data.results[0].alternatives[0].transcript);
							});
						};
					}
				};
				// start recording with 1 second time between receiving 'ondataavailable' events
				microphone.start(1000);
				$_(this).html ('<span class="fa fa-stop"></span>');
			}).catch((err) => {
				console.error (err);
			});
		}
	});
});
