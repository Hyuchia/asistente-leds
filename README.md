# Asistente IoT Leds

## Equipo

Alejandra Cuéllar González A01333324

Cinthya Daniela Lugo Novoa A01332942

Diego Islas Ocampo A01332956

Luis Fernando Saavedra Meza A01333410

## Instrucciones

### Visualización
El asistente cuenta con una visualización de software y una de hardware.
La visualización de software consiste en 3 puntos que se iluminarán representando los colores rojo, verde y azul así como uno que muestra la combinación de los tres valores.
La visualización por hardware fue realizada con un Arduino UNO y una protoboard con 3 Leds representando los mismos colores.

### Comandos

Entidades:

@color: rojo, verde o azul

@number: 0 - 255

@hex: Color en formato hexadecimal (ex. #ffffff)


El asistente puede entender los siguientes comandos:

* `prende el foco @color`
* `apaga el foco @color`

* `haz parpadear el foco @color`
* `deten el foco @color`

* `pon el color @hex`
* `pon el foco rojo en @number`

## Evidencias
En este repositorio se incluyen fotos como evidencia del funcionamiento de la aplicación. Cada acción efectuada tiene una foto que demuestra su funcionalidad en hardware y software cuyos numeros corresponden.

### Paso 0
![Software 0](./Software 0.png)
![Hardware 0](./Hardware 0.jpg)

### Paso 1
![Software 1](./Software 1.png)
![Hardware 1](./Hardware 1.jpg)

### Paso 2
![Software 2](./Software 2.png)
![Hardware 2](./Hardware 2.jpg)

### Paso 3
![Software 3](./Software 3.png)
![Hardware 3](./Hardware 3.jpg)

### Paso 4
![Software 4](./Software 4.png)
![Hardware 4](./Hardware 4.jpg)

### Paso 5
![Software 5](./Software 5.png)
![Hardware 5](./Hardware 5.jpg)
![Hardware 6](./Hardware 6.jpg)

### Paso 6
![Software 6](./Software 6.png)
![Hardware 7](./Hardware 7.jpg)
